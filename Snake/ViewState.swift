//
//  ViewState.swift
//  Snake
//
//  Created by vikingosegundo on 23/10/2021.
//

import SwiftUI

final class ViewState:ObservableObject {
    @Published var game:Game
    @Published var target:Coordinate?

    init(game g:Game) {
        game = g
        game.register { self.game = g }
    }
}
