//
//  Snake.swift
//  Snake
//
//  Created by vikingosegundo on 28/10/2021.
//

struct Snake {
    init (head:Coordinate) { self.init(head, [], .north) }
    
    enum Change {
        case move(Move)
        case grow
        enum Move   { case forward, right, left }
    }
    enum Facing { case north, east, south, west }
    
    let head  : Coordinate
    let tail  : [Coordinate]
    let facing: Facing
    var body  : [Coordinate] {  return [head] + tail }

    func alter(_ c:Change) -> Self {
        switch c {
        case let .move(d): return move(d)
        case     .grow   : return grow( )
        }
    }
}
private extension Snake {
    init(_ h:Coordinate,_ t:[Coordinate],_ f:Facing) { head = h; tail = t; facing = f }
    func move(_ move:Change.Move) -> Self {
        var newTail: [Coordinate] { Array(body.prefix(tail.count)) }
        switch (facing, move) { //         |<-------------- head -------------->|  tail  |facing|
        case (.north,.forward): return Self(Coordinate(x:head.x    ,y:head.y - 1),newTail,.north)
        case (.east ,.forward): return Self(Coordinate(x:head.x + 1,y:head.y    ),newTail,.east )
        case (.south,.forward): return Self(Coordinate(x:head.x    ,y:head.y + 1),newTail,.south)
        case (.west ,.forward): return Self(Coordinate(x:head.x - 1,y:head.y    ),newTail,.west )
        case (.north,   .left): return Self(Coordinate(x:head.x - 1,y:head.y    ),newTail,.west )
        case (.east ,   .left): return Self(Coordinate(x:head.x    ,y:head.y - 1),newTail,.north)
        case (.south,   .left): return Self(Coordinate(x:head.x + 1,y:head.y    ),newTail,.east )
        case (.west ,   .left): return Self(Coordinate(x:head.x    ,y:head.y + 1),newTail,.south)
        case (.north,  .right): return Self(Coordinate(x:head.x + 1,y:head.y    ),newTail,.east )
        case (.east ,  .right): return Self(Coordinate(x:head.x    ,y:head.y + 1),newTail,.south)
        case (.south,  .right): return Self(Coordinate(x:head.x - 1,y:head.y    ),newTail,.west )
        case (.west ,  .right): return Self(Coordinate(x:head.x    ,y:head.y - 1),newTail,.north)
        }
    }
    func grow() -> Self {
        //  |head|<----------------- tail ----------------->|facing|
        Self(head,!tail.isEmpty ? tail+[tail.last!] : [head],facing)
    }
}
