//  Created by vikingosegundo on 22/5/2021.

import SwiftUI

struct SnakeButton:View {
    @StateObject var state:ViewState
    let title:String
    let strokeColor:Color
    let backgroundColor:Color

    let callback: ()->()
    var game: Game { state.game }
    
    var body: some View {
        VStack {
            Button { callback() }
        label: { Text(title)
            .frame(width:100, height:100)
            .tint(.white)
            .background(backgroundColor)
            .overlay     (Circle().stroke(strokeColor, style:StrokeStyle(lineWidth:1, dash:[5.0])))
            .clipShape   (Circle())
            .contentShape(Circle())
        }
        }
    }
}

struct SnakeBoard:View {
    func bodyAndTarget() -> [Coordinate] {
        (game.target() != nil
            ? [ game.target()! ]
            : [])
        + game.snake().body
    }
    @StateObject var state:ViewState
    let pixelFactor:Double
    var game: Game { state.game }
    var body:some View {
        Canvas { ctx, size in
            bodyAndTarget().forEach {
                let color =
                $0 == (game.target() != nil ? game.target()! : .init(x: -1, y: -1))
                    ? Color.red : .white
                let path = Path(CGRect(origin:CGPoint(x:$0.x * Int(pixelFactor), y:$0.y * Int(pixelFactor)), size:.init(width:pixelFactor, height:pixelFactor)))
                ctx.stroke(path, with:.color(color))
                ctx.fill  (path, with:.color(color))
            }
        }
        .background(Color(white: 0.1))
        .frame(width: Double(state.game.size().width) * pixelFactor, height: Double(state.game.size().height) * pixelFactor, alignment: .center)
    }
}

struct ContentView:View {
    @StateObject var state:ViewState
    let pixelFactor:Double
    var game: Game { state.game }
    
    var body:some View {
        LazyVStack {            
            LazyHStack {
                button(for:.left)
                SnakeBoard(state:state, pixelFactor:pixelFactor)
                button(for:.right) } }
        .frame(alignment:.center)
    }
    
    private
    func button(for direction:Snake.Change.Move) -> SnakeButton {
        .init(    state: state,
                  title: game.state() == .ongoing ? "  " : "new",
            strokeColor: game.state() == .ongoing ? .white : .orange,
        backgroundColor: Color(white: 0.1, opacity: 1.0), callback: game.state() == .ongoing ? {game.command(.move(direction))} : {game.command(.reset)} )
    }
}
