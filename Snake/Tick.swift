//
//  Tick.swift
//  Snake
//
//  Created by vikingosegundo on 29/10/2021.
//

import Foundation.NSTimer

final
class Tick {
    enum Change {
        case set(_Callback);  enum _Callback {
            case interval(to:Time)
            case callback(() -> ()) }
        case invalidate
    }
    convenience init() { self.init(interval:1, callback:{}) }
    func configure(_ changes:Change...) { changes.forEach { self.alter($0) } }
    
    private var interval:Time
    private var callback:(() -> ())
    private var timer:Timer?
    
    private
    init(interval i:Time,
         callback c: @escaping (() -> ()))
    {
        interval = i
        callback = c
        configure()
    }
    private
    func configure() {
        timer = .scheduledTimer(withTimeInterval:interval, repeats:false, block:{ _ in self.callback() } )
    }
    private
    func alter(_ change:Change)  {
        switch change {
        case .invalidate: timer?.invalidate(); timer = nil
        case .set(.interval(to: let i)):timer?.invalidate(); interval = i; timer = .scheduledTimer(withTimeInterval:interval, repeats:false, block:{_ in self.callback() })
        case .set(.callback(    let c)):timer?.invalidate(); callback = c; timer = .scheduledTimer(withTimeInterval:interval, repeats:false, block:{_ in self.callback() })
        }
    }
}
