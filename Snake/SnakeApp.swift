//
//  SnakeApp.swift
//  Snake
//
//  Created by vikingosegundo on 22/10/2021.
//

import SwiftUI

@main
struct SnakeApp:App {
    var body:some Scene {
        WindowGroup {
            ZStack {
                Color.black
                    .edgesIgnoringSafeArea(.all)
                ContentView(state:ViewState(
                    game:SnakeGame.create(size: (90,50))),pixelFactor:5.0)
            }
        }
    }
}
