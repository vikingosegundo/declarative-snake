//  Created by vikingosegundo on 22/10/2021.

enum SnakeGame {
    static func create(size:Size) -> Game {
        let stepTick  : Tick = .init()
        let targetTick: Tick = .init()
        var observers : [() -> ()] = []
        var target    : Coordinate?
        var state     : State = .ongoing { didSet { observers.forEach{ $0() } } }
        var snake     = Snake(head:Coordinate(x:size.width / 2, y:size.height / 2)) {
            didSet {
                observers.forEach{ $0() }
                if target != nil { if snake.head == target { snake = snake.alter(.grow); executeTargetEvent() } }
            }
        }
        func eval(command c:Command)   {
            switch c {
            case .reset: {
                state = .ongoing
                snake = Snake(head:Coordinate(x:size.width / 2, y:size.height / 2)); run()}()
            case .move(let m):
                guard state == .ongoing else { return }
                snake = snake.alter(.move(m))  }
        }
        func executeStepEvent ()       {
            func checkHeadIsOutOfBounds() -> Bool { snake.head.x < 0 || snake.head.y < 0 || snake.head.x == size.width || snake.head.y == size.height }
            func checkHeadIsInTail     () -> Bool { snake.tail.count > 4 && snake.tail.contains { snake.head == $0 }      }
            stepTick.configure(
                .set(.interval(to:snake.speedFactor())),
                .set(
                    .callback({
                        checkHeadIsOutOfBounds() || checkHeadIsInTail()
                            ? state = .ended
                            : ()
                        state == .ongoing
                            ? { snake = snake.alter(.move(.forward)); executeStepEvent() }()
                            : { stepTick.configure(.invalidate)                          }() })))
        }
        func executeTargetEvent()      {
            func setFireTimeForTargetEvent() {
                targetTick.configure(
                    .set(.interval(to:Time(20 - min(snake.tail.count, 15)))) )
            }
            func randomAvailableCoordinate() -> Coordinate {
                Set(Array(0..<size.width * size.height).map { Coordinate(x:$0 % size.width, y:$0 / size.width) })
                    .subtracting(snake.body)
                    .randomElement() ?? .init(x:-1, y:-1)
            }
            state == .ongoing
            ? { target = randomAvailableCoordinate(); setFireTimeForTargetEvent() }()
            : { targetTick.configure(.invalidate)                                }()
        }
        func setTargetEventCallback()  {
            targetTick.configure(.set(.callback({ executeTargetEvent() })) )
        }
        func run() {
            setTargetEventCallback()
            executeTargetEvent()
            executeStepEvent()
        }
        run()
        return ( snake   : { snake                        },
                 size    : { size                         },
                 state   : { state                        },
                 target  : { target                       },
                 command :   eval(command:),
                 register: { observers = observers + [$0] })
    }
}


struct Coordinate { let x, y:Int }
typealias Size = (width:Int, height:Int)
enum Command {
    case reset
    case move(Snake.Change.Move)
}
enum State { case ongoing, ended }
typealias Game = (
    snake   :(                    ) -> Snake,
    size    :(                    ) -> Size,
    state   :(                    ) -> State,
    target  :(                    ) -> Coordinate?,
    command :(       Command      ) -> (),
    register:( @escaping () -> () ) -> ()
)

extension Coordinate:Equatable { }
extension Coordinate:Hashable { }

fileprivate
extension Snake {
    func speedFactor() -> Double {
        let d = 3
        switch tail.count {
        case 0*d ..<  1*d: return 1.0 /  9.0
        case 1*d ..<  2*d: return 1.0 / 12.0
        case 2*d ..<  3*d: return 1.0 / 15.0
        case 3*d ..<  4*d: return 1.0 / 18.0
        case 4*d ..<  5*d: return 1.0 / 21.0
        case 5*d ..<  6*d: return 1.0 / 24.0
        case 6*d ..<  7*d: return 1.0 / 27.0
        case 7*d ..< .max: return 1.0 / 30.0
        default: return 0
        }
    }
}
