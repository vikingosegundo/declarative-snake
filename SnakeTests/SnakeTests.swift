//
//  SnakeTests.swift
//  SnakeTests
//
//  Created by vikingosegundo on 27/10/2021.
//

@testable import Snake
import Quick
import Nimble

class SnakeSpec: QuickSpec {
    override func spec() {
        context("Snake") {
            let snake = Snake(head:Coordinate(x:5,y:5))
            context("newly created") {
                it("faces north"     ) { expect(snake.facing    ) == .north }
                it("has no tail"     ) { expect(snake.tail.count) == 0      }
                it("initial position") { expect(snake.head).to(equal(Coordinate(x: 5,y: 5))) }
            }
            context("move") {
                context("forward") {
                    let snake = snake.alter(.move(.forward))
                    it("still faces north") { expect(snake.facing) == .north }
                    it("has no tail"      ) { expect(snake.tail.count) == 0      }
                    it("moved up one step") { expect(snake.head).to(equal(Coordinate(x: 5,y: 4))) }
                }
                context("right") {
                    let snake = snake.alter(.move(.right))
                    it("now faces east"                     ) { expect(snake.facing) == .east }
                    it("has no tail"                        ) { expect(snake.tail.count) == 0 }
                    it("moved right one step"               ) { expect(snake.head).to(equal(Coordinate(x: 6,y: 5))) }
                    context("right again"                   ) { let snake = snake.alter(.move(.right))
                        it("now faces south"                ) { expect(snake.facing) == .south }
                        it("has no tail"                    ) { expect(snake.tail.count) == 0      }
                        it("moved right one step"           ) { expect(snake.head).to(equal(Coordinate(x: 6,y: 6))) }
                        context("right again"               ) { let snake = snake.alter(.move(.right))
                            it("now faces west"             ) { expect(snake.facing) == .west }
                            it("has no tail"                ) { expect(snake.tail.count) == 0      }
                            it("moved right one step"       ) { expect(snake.head).to(equal(Coordinate(x: 5,y: 6))) }
                            context("right again"           ) { let snake = snake.alter(.move(.right))
                                it("now faces north"        ) { expect(snake.facing) == .north }
                                it("has no tail"            ) { expect(snake.tail.count) == 0      }
                                it("moved back to the start") { expect(snake.head).to(equal(Coordinate(x: 5,y: 5))) }
                            }
                        }
                    }
                }
                context("left") {
                    let snake = snake.alter(.move(.forward))
                        .alter(.move(.left))
                    it("now faces west"                                 ) { expect(snake.facing) == .west }
                    it("has no tail"                                    ) { expect(snake.tail.count) == 0      }
                    it("moved left one step"                            ) { expect(snake.head).to(equal(Coordinate(x: 4,y: 4))) }
                    context("forward"                                   ) { let snake = snake.alter(.move(.forward))
                        context("left again"                            ) { let snake = snake.alter(.move(.left))
                            it("now faces south"                        ) { expect(snake.facing) == .south }
                            it("has no tail"                            ) { expect(snake.tail.count) == 0      }
                            it("moved left one step"                    ) { expect(snake.head).to(equal(Coordinate(x: 3,y: 5))) }
                            context("forward"                           ) { let snake = snake.alter(.move(.forward))
                                context("left again"                    ) { let snake = snake.alter(.move(.left))
                                    it("now faces east"                 ) { expect(snake.facing) == .east }
                                    it("has no tail"                    ) { expect(snake.tail.count) == 0      }
                                    it("moved left one step"            ) { expect(snake.head).to(equal(Coordinate(x: 4,y: 6))) }
                                    context("forward"                   ) { let snake = snake.alter(.move(.forward))
                                        context("left again"            ) { let snake = snake.alter(.move(.left))
                                            it("now faces north"        ) { expect(snake.facing) == .north }
                                            it("has no tail"            ) { expect(snake.tail.count) == 0      }
                                            it("moved back to the start") { expect(snake.head).to(equal(Coordinate(x: 5,y: 5))) }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            context("grow") {
                let snake = snake.alter(.grow)
                it("has tail of size 1") {expect(snake.tail.count) == 1}
            }
        }
    }
}
